class VehicleModelSerializer

  include JSONAPI::Serializer
  attributes :name

  link :vehicle_brand

end
