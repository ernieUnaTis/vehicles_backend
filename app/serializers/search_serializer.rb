class SearchSerializer
  include JSONAPI::Serializer

  attributes :price,
             :mileage,
             :year,
             :model,
             :brand

  attribute :model do |object|
    @vehicle_model = VehicleModel.find(object.vehicle_model_id).name
  end

  attribute :brand do |object|
    VehicleModel.find(object.vehicle_model_id).vehicle_brand.name
  end

end