class VehicleBrandSerializer
  include JSONAPI::Serializer
  attributes :id,
             :name
end
