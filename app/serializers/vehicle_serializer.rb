class VehicleSerializer
  include JSONAPI::Serializer
  attributes :price,
             :mileage,
             :year,
             :model,
             :id

  attribute :model do |object|
    @vehicle_model = VehicleModel.find(object.vehicle_model_id)
    ::VehicleModelSerializer.new(@vehicle_model)
  end


end
