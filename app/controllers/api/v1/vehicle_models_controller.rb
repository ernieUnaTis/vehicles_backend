class Api::V1::VehicleModelsController < Api::ApiController
  before_action :set_vehicle_brand, only: [ :create ]

  def create
    @vehicle_model = @vehicle_brand.vehicle_model.build(vehicle_model_params.except(:brand))
    if @vehicle_model.save
      render json: VehicleModelSerializer.new(@vehicle_model), status: :ok
    else
      render json: ErrorSerializer.serialize(@vehicle_model.errors), status: :bad_request
    end
  end

  private

  def set_vehicle_brand
    validate_brand_not_empty(params[:brand])
    @vehicle_brand = VehicleModel.validate_brand(vehicle_model_params)
  end

  def validate_brand_not_empty(brand)
    if(brand=="")
      render json: '{"estatus":"Brand no puede ir vacía"}', status: :bad_request
    end
  end

  def vehicle_model_params
    params.permit(:name,
                  :brand)
  end

  def set_vehicle_model
    @vehicle_model = VehicleModel.find(params[:id])
  end
end
