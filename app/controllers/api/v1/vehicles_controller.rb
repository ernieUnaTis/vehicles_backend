class Api::V1::VehiclesController < Api::ApiController
  before_action :set_vehicle_model, only: [ :create ]

  def create
    @vehicle = @vehicle_model.vehicles.build(vehicle_params.except(:model, :brand))
    if @vehicle.save
      render json: VehicleSerializer.new(@vehicle), status: :ok
    else
      render json: ErrorSerializer.serialize(@vehicle.errors), status: :bad_request
    end
  end

  def search
    puts "TEXT" + params["text"].to_s
    puts "Category" + params["category"].to_s

    if(params["category"].to_i==3)
      @vehicles = Vehicle.by_year(params["text"])
    end

    if(params["category"].to_i==4)
      @vehicles = Vehicle.by_price(params["text"])
    end

    if(params["category"].to_i==5)
      @vehicles = Vehicle.by_mileage(params["text"])
    end

    if(params["category"].to_i==2)
      brands = VehicleBrand.where("name LIKE ?","#{params["text"].to_s}%")
      @vehicles = Vehicle.by_model(brands[0].vehicle_model.ids)
    end

    if(params["category"].to_i==1)
      models = VehicleModel.where("name LIKE ?","#{params["text"].to_s}%")
      @vehicles = Vehicle.by_model(models.ids)
    end

    if(params["category"].to_i==0)
      @vehicles = Vehicle.all
    end

    render json: SearchSerializer.new(@vehicles), status: :ok

  end

  private
  def vehicle_params
    params.permit(:model,
                  :brand,
                  :year,
                  :mileage,
                  :price)
  end

  def set_vehicle_model
    @vehicle_model = Vehicle.validate_model(vehicle_params)
  end

  def set_vehicle
    @vehicle = Vehicle.find(params[:id])
  end
end
