class VehicleModel < ApplicationRecord
  belongs_to :vehicle_brand
  has_many :vehicles
  validates :name, uniqueness: { message: 'Vehiculo ya creado' }
  validates :name, presence: { message: 'Debe agregar nombre del Vehiculo' }

  def self.validate_brand(params)
    vehicle_brand = VehicleBrand.find_by(:name => params[:brand])
    if vehicle_brand.nil?
      vehicle_brand = VehicleBrand.new
      vehicle_brand.name = params[:brand]
      vehicle_brand.save!
    end
    return vehicle_brand
  end



end
