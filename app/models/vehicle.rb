class Vehicle < ApplicationRecord
  belongs_to :vehicle_model
  validates :price, presence: { message: 'price no puede ir vacío' }
  validates :mileage, presence: { message: 'mileage no puede ir vacío' }
  validates :year, presence: { message: 'Year no puede ir vacío' }


  def self.validate_model(params)
    vehicle_model = VehicleModel.find_by(:name => params[:model])
    return vehicle_model
  end

  scope :by_model, -> (vehicle) {
    where('vehicle_model_id IN (?)',vehicle)
  }

  scope :by_brand, -> (vehicles_by_brand) {
    where('vehicle_model_id IN (?)',vehicles_by_brand)
  }

  scope :by_year, -> (year) {
      where('year > (?)',year)
  }

  scope :by_mileage, -> (mileage) {
    where('mileage < (?)',mileage)
  }

  scope :by_price, -> (price) {
    where('price < ?',price)
  }
end
