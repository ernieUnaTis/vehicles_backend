Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :vehicle_models do
        resources :vehicles, only: [:create]
      end
      resources :vehicles, only: [:show, :edit, :update, :destroy]
      post 'search_vehicles', to: 'vehicles#search'
    end
  end
end
